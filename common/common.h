/*
 * =====================================================================================
 *
 *       Filename:  common.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  08.09.2010 11:01:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Georg Wassen (gw) (), 
 *        Company:  Lehrstuhl für Betriebssysteme, RWTH Aachen University
 *                  http://www.lfbs.rwth-aachen.de
 *
 * =====================================================================================
 */
#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>

/* default ports */
#define COMMON_ADMIN_PORT    12301
#define COMMON_OBSERVER_PORT 12302
#define COMMON_ROBOT_PORT    12303

/* Types of Items */
enum item_type {item_coin, item_beamport, item_start, item_finish};
typedef enum item_type item_type_t;

/* Magic Numbers for Packages Identification */
#define COMMON_DYNAMIC_MAGIC  0x18F2D27B
#define COMMON_STATIC_MAGIC   0x98F2D27C
#define COMMON_MESSAGE_MAGIC  0x98EAD27B

/* structs for observer binary protocol data elements */
/*    Attention!
 *    most elemente are scaled (see data.c:update_scene() for details)
 */
const static int obp_common_header_size = 8;
struct obp_dynamic_base {
    uint32_t magic;
    union {
        struct {
            unsigned char dummy[3]; //  0..23
            unsigned char robot;    // 24..31
        } __attribute__ ((packed)) cnt;
        struct {                // it's a bit field!
            uint32_t hsec :7;       //  0..6
            uint32_t sec :6;        //  7..12
            uint32_t min :6;        // 13..18
            uint32_t hour :5;       // 19..23
            // uint32_t dummy :8;   // 24..31
        } time __attribute__ ((packed));
        uint8_t u8[4];
        uint16_t u16[2];
        uint32_t u32;
    } data __attribute__ ((packed));
} __attribute__ ((packed));
struct obp_dynamic_robot {
    uint16_t pos_x;
    uint16_t pos_y;
    uint16_t heading;               /* is normalized to 0..2*PI in data.c (here: 000 .. 36000 1/100°) */
    uint16_t target_heading;
    int16_t speed;                  /* can be negative */
    uint16_t coins;
} __attribute__ ((packed));
struct obp_static_base {
    uint32_t magic;
    uint8_t cnt_wall;
    uint8_t cnt_item;
    uint8_t cnt_robot;
    uint8_t dummy1;
    uint16_t size_x;
    uint16_t size_y;
} __attribute__ ((packed));
struct obp_static_wall {
    uint16_t pos_x1;
    uint16_t pos_y1;
    uint16_t pos_x2;
    uint16_t pos_y2;
} __attribute__ ((packed));
struct obp_static_item {
    uint16_t type;
    uint16_t pos_x;
    uint16_t pos_y;
    uint16_t pos_x2;
    uint16_t pos_y2;
    uint16_t radius;
    uint16_t value;
    char name[16];
} __attribute__ ((packed));
struct obp_static_robot {
    uint64_t id;
    char name[16];
    int16_t total_dist;
    uint16_t total_time;
    uint16_t total_collisions;
    uint16_t count_dist;
    uint16_t count_line;
    uint16_t count_sector;
} __attribute__ ((packed));


#endif

