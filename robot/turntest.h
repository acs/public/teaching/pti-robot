/*
 * #######################################################################################
 * ##    Filename:  turntest.h
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#ifndef TURNTEST_H
#define TURNTEST_H

#include "behavior.h"

class TurnTest : public Behavior
{
    public:
        explicit TurnTest(std::string param);

        virtual void start();
        virtual void stop();

    private:
        float p_speed;
};

#endif // TURNTEST_H
