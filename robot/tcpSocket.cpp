/*
 * #######################################################################################
 * ##    Filename:  tcpSocket.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##      Author:  Felix Wege (ACS)
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##
 * #######################################################################################
 */

#include"tcpSocket.h"

#include <sys/types.h>
#ifdef WIN32
#undef UNICODE
#include <ws2tcpip.h> // getaddinfo
#include <winsock2.h>
// link with Ws2_32.lib
#pragma comment(lib, "Ws2_32.lib")
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <unistd.h>
#define closesocket close
#include <string.h> // memcpy
#endif

#include <iostream>

/*
* Default-Constructor
*/
TcpSocket::TcpSocket(){
    // nothing to do
}

/*
* Constructor
*
* Params:
*   serveraddress    address of the host
*   port             port of the host
*
* Initialize socket struct if serveraddress and port already passed
*/
TcpSocket::TcpSocket(const char* serveraddress, int port){
    init(serveraddress, port);
}

/*
* Destructor
*
* Close socket, if still connected and free socket struct
*/
TcpSocket::~TcpSocket(){
    if(connected) _close();
    delete(sockAddrPtr);
}

/*
* init()
*
* Params:
*   serveraddress    address of the host
*   port             port of the host
*
* Initialize socket struct and fill with required information
*/
void TcpSocket::init(const char* serveraddress, int port){
        // Initiliaze socket struct with required information
    sockAddrPtr = new struct sockaddr_in();

#ifdef WIN32
    // Windows muss da erst initialisieren!
    WSADATA wsaData;
    int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (result != 0) {
        printf("WSAStartup failed: %d\n", result);
        return 1;
    }
    sockAddrPtr->sin_addr.s_addr = inet_addr(serveraddress);
#else
    struct hostent *serverID;
    serverID = gethostbyname(serveraddress);
    if (serverID == 0)
    {
        std::cout << "ERROR: Unknown host " << serveraddress << std::endl;
        return;
    }
    memcpy(&sockAddrPtr->sin_addr, serverID->h_addr, serverID->h_length);
#endif

    sockAddrPtr->sin_port = htons(port);
    sockAddrPtr->sin_family = AF_INET;

    initialized = true;
}


/*
* connectToHost()
*
* Connect socket to host based on previously initialized socket struct
*/
int TcpSocket::connectToHost(){
    if(!initialized){
        std::cout << "ERROR: Socket not initiliazed."
                  << " Call init() before or use connectToHost(const char* serveraddress, int port) instead"
                  << std::endl; 

        return -1;
    }
    return(_open());
}

/*
* connectToHost(const char* serveraddress, int port)
*
* Params:
*   serveraddress    address of the host
*   port             port of the host
*
* Closes the socket, i.e. it disconnects it from the host
*/
int TcpSocket::connectToHost(const char* serveraddress, int port){
    if(initialized){
        std::cout << "WARNING: Socket already initialized, settings will be overwritten!" << std::endl
                  << "If this is not wished, use connectToHost() instead..." << std::endl;

    }

    init(serveraddress, port);
    return(_open());
}

/*
* disconnectFromHost()
*
* Disconnect from host by closing the used socket
*/
int TcpSocket::disconnectFromHost(){
    return(_close());
}


/*
* _open()
*
* Connects socket to host
*/
int TcpSocket::_open(){
    if(connected || !initialized){
        std::cout << "ERROR: open() - cannot open socket! " <<
           "Either the socket was already opened or it was not initialized" << std::endl;
        return -1;
    }

    _socket = socket(AF_INET, SOCK_STREAM, 0);
    if(_socket <= 0) {
        std::cout << "ERROR: open() - socket() returned " << _socket << std::endl;
        return -1;
    }

    struct sockaddr_in AdrSock = *sockAddrPtr;
    
    int error = connect(_socket, (struct sockaddr *)&AdrSock, sizeof(AdrSock));
    if(error < 0){
        std::cout << "ERROR: open() - connect() returned " << error << std::endl;
        return error;
    }    

    connected = true;
    return 1;
}

/*
* _close()
*
* Closes the socket, i.e. it disconnects it from the host
*/
int TcpSocket::_close(){
    if(connected){
        closesocket(_socket);
        return 1;
    } else {
        std::cout << "ERROR: close() - socket not opened" << std::endl;
        return -1;
    }
}

/*
* _sendString()
*
* Params:
*   _message        string containing the message to be sent
*
* Sends a string to the remote host
*/
void TcpSocket::sendString(std::string _message){

    // FIXME: Appending line terminator does not work if the string already ends on "\n"
    if(strcmp(&_message.back(),"\n") != 0){
            _message.append("\n");
    }

    // Cast to char array to send to socket
    char message[MAXMSG];
    strcpy(message, _message.c_str());
    
    // Send message to socket
    int error = send(_socket, message, strlen(message), 0);
    if (error < 0){
        std::cout << "ERROR: dialog() - send() returned " << error << std::endl;
    }
}

/*
* receiveString()
*
* Receives a string from the remote host
*/
std::string TcpSocket::receiveString(){
    char buffer[MAXBUFF] = "";
    // Receive message
    int len = recv(_socket, buffer, MAXBUFF, 0);
    // Return message
    return (std::string(buffer));
}

/*
* dialog()
*
* Params:
*   _message        string containing the message to be sent
*
* Sends a string to server and returns its answer.
*
* The send-string will get an EOL suffix, if none exists.
* The maximum  sizes of incoming and outgoing messages can be
* defined in tcpSocket.h
*/
std::string TcpSocket::dialog(std::string _message){
    sendString(_message);
    return(receiveString());
}