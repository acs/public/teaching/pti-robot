/*
 * #######################################################################################
 * ##    Filename:  cli.h
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##      Author:  Felix Wege (ACS)
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##
 * #######################################################################################
 */

#ifndef CLI_H
#define CLI_H

#include <string>
#include <list>

#include "behavingrobot.h"

class CLI {
    public:
        // Constructors + Destructor
        CLI();
        CLI(std::string _robotName, std::string _serverAddress, int _serverPort);
        ~CLI();

        // Initialization and start
        void init();
        void startCLI();
        
        // Logging
        void logString(std::string message);

    private:
        // Displaying functions
        void printScene(std::string fixedMessage = "");
        void clearScene();
        void updateScene(std::string fixedMessage = "");
        // Logging
        void setLogSize(unsigned int size);
        // Interaction
        std::string getUserInput();
        void processInput(std::string input);
        void waitForUser();

        // First level functions
        void sensors();     // Displays all sensor commands
        void commSeq();     // Displays all command sequence commands
        void behavior();    // Displays all behavior commands
        void tBehavior();   // Displays all timed behavior commands
        void telnet();      // Starts telnet emulation

        // Second level functions
        //// sensors
        float distance();    // Returns travelled distance of robot
        void rDistance();   // Resets distance sensor
        float line(double angle);        // Calls specific distance sensor
        void aLine();       // Calls all line sensors
        float sector(int sec);      // Calls sector sensor
        float speed();       // Returns current robot speed
        //// commSeq
        void simple();      // Sends simple command sequence to robot
        void measureLat();  // Measures latencies for different routines
        //// behavior
        void brakeTest(std::string param);       // Starts BrakeTest behavior
        void turnTest(std::string param);        // Starts TurnTest Behavior
        void targetBraking(std::string param);   // Starts TargetBraking behavior
        //// tBehavior
        void tTargetBraking(std::string param);  // Starts TimedTargetBraking behavior
        void tBrakeControl(std::string param);   // Starts TimedBrakeControl behavior
        void followWall(std::string param);      // Starts FollowWall behavior
        void speedControl(std::string param);    // Starts SpeedControl behavior
        void tStop();                            // Stops current behavior

        // General CLI parameters
        bool running = false; 

        // Displaying parameters
        float speedVal = 0.0;
        float maxSpeed = 16.0; // Paramteter defined in arena --> DO NOT CHANGE UNASKED!
        
        // Logging parameters
        int LOGSIZE;
        std::list<std::string> log;
        
        // Robot parameters
        // BehavingRobot robot;
        BehavingRobot* p_robot;
        std::string robotName;
        std::string serverAddress;
        int serverPort;

        // First level command list
        const std::string commands = std::string("\n | Commands: |\n\n")
                                   + std::string(" sensors  --> list sensor commands  | commSeq   --> list command sequences\n")
                                   + std::string(" behavior --> list behaviors        | tBehavior --> list timed behaviors\n")
                                   + std::string(" telnet   --> telnet emulator       | logsize   --> change size of log\n")
                                   + std::string(" W/A/S/D   --> manual control       | exit      --> exit CLI");

    //....
};


#endif /* CLI_H */