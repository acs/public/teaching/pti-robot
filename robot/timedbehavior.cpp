/*
 * #######################################################################################
 * ##    Filename:  timedbehavior.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#include "timedbehavior.h"
#include <pthread.h>


TimedBehavior::TimedBehavior(std::string param) :
    Behavior(param),
    p_freq(100)
{}


void TimedBehavior::start()
{
    init();
    timer.setInterval([&](){
        task();
    },1000/p_freq);   
}

void TimedBehavior::stop()
{
    timer.stopTimer();
    finalize();
}

