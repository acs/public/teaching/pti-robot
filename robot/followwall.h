/*
 * #######################################################################################
 * ##    Filename:  followwall.h
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#ifndef FOLLOWWALL_H
#define FOLLOWWALL_H

#include "timedbehavior.h"

class FollowWall : public TimedBehavior
{
public:
    explicit FollowWall(std::string param);

private:
    virtual void init();
    virtual void task();
    virtual void finalize();

    float p_speed;
    float p_dist;
};

#endif // FOLLOWWALL_H
