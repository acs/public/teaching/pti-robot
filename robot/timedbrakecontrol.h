/*
 * #######################################################################################
 * ##    Filename:  timedbrakecontrol.h
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#ifndef TIMEDBRAKECONTROL_H
#define TIMEDBRAKECONTROL_H

#include "timedbehavior.h"

class TimedBrakeControl : public TimedBehavior
{
public:
    explicit TimedBrakeControl(std::string param);

private:
    virtual void init();
    virtual void task();
    virtual void finalize();

private:
    float p_max_speed;

};

#endif // TIMEDBRAKECONTROL_H
