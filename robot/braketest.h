/*
 * #######################################################################################
 * ##    Filename:  braketest.h
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#ifndef BRAKETEST_H
#define BRAKETEST_H

#include "behavior.h"

class BrakeTest : public Behavior
{
public:
    explicit BrakeTest(std::string);

    virtual void start();
    virtual void stop();

private:
    float p_speed;          // Zielgeschwindigkeit (wird vom Konstruktor gesetzt)

};

#endif // BRAKETEST_H
