/*
 * #######################################################################################
 * ##    Filename:  timedtargetbraking.h
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#ifndef TIMEDTARGETBRAKING_H
#define TIMEDTARGETBRAKING_H

#include "timedbehavior.h"

class TimedTargetBraking : public TimedBehavior
{
public:
    explicit TimedTargetBraking(std::string param);

private:
    virtual void init();
    virtual void task();
    virtual void finalize();

    float p_speed;
    float p_dist;
};

#endif // TIMEDTARGETBRAKING_H
