/*
 * #######################################################################################
 * ##    Filename:  targetbraking.h
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#ifndef TARGETBRAKING_H
#define TARGETBRAKING_H

#include "behavior.h"

class TargetBraking : public Behavior
{
public:
    explicit TargetBraking(std::string param);

    virtual void start();
    virtual void stop();

private:
    float p_speed;          // Zielgeschwindigkeit (wird vom Konstruktor gesetzt)

};

#endif // TARGETBRAKING_H
