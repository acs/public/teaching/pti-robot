/*
 * #######################################################################################
 * ##    Filename:  timedbehavior.h
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#ifndef TIMEDBEHAVIOR_H
#define TIMEDBEHAVIOR_H

#include "timercpp.h"
#include "behavior.h"

class TimedBehavior : public Behavior
{
public:
    explicit TimedBehavior(std::string param);

    virtual void start();
    virtual void stop();

private:
    virtual void init() = 0;
    virtual void task() = 0;
    virtual void finalize() = 0;

protected:
    unsigned p_freq;
    Timer timer;

};

#endif // TIMEDBEHAVIOR_H
