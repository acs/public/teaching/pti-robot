/*
 * #######################################################################################
 * ##    Filename:  turntest.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#include "turntest.h"
#include<unistd.h>
#include<cmath>

TurnTest::TurnTest(std::string param) :
    Behavior(param)
{
    p_speed = std::stof(p_param);
}

void TurnTest::start()
{
    float startturn, dist;

    p_robot->distanceReset();
    p_robot->speed(p_speed);

    while ((startturn = p_robot->distance()) < 30.0) {};
    p_robot->turn(-90);

    usleep(3e6);
    p_robot->speed(.0);

    while (std::abs(dist - p_robot->distance()) > 1e-6) {
        dist = p_robot->distance();
        usleep(100e3);
    }

    p_robot->message("TurnTest: speed=" + std::to_string(p_speed) + " drift:$(Y-" + std::to_string(startturn) + ")");
    p_robot->turn(-90);
}

void TurnTest::stop()
{

}
