/*
 * #######################################################################################
 * ##    Filename:  braketest.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS)
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#include "braketest.h"

#include<iostream>
#include<cmath>
#include<unistd.h>


/*******************************************************************
 *
 * BrakeTest::BrakeTest()
 *
 *------------------------------------------------------------------
 * Parameter:
 *   QString param       Zeichenkette mit Parametern (s.u.)
 *------------------------------------------------------------------
 * Der Konstruktor initialisiert interne Variablen mit Werten,
 * die als Zeichenkette param übergeben werden können.
 * Der Funktionsprototyp für diese Zeichenkette ist:
 *           f(float speed = 4.0)
 * Es gibt also einen optionalen Parameter mit Standardwert 4.0,
 * der die Geschwindigkeit angibt.
 *******************************************************************/
BrakeTest::BrakeTest(std::string param) :
    Behavior(param)
{
    p_speed = std::stof(p_param);
}

/*******************************************************************
 *
 * void BrakeTest::start()
 *
 *------------------------------------------------------------------
 * Parameter:
 *   none
 * Rückgabewert:
 *   none
 *------------------------------------------------------------------
 * Die Startmethode wird beim Aktivieren dieses Verhaltens aufgerufen.
 * So lange diese Methode läuft, wird das Programm nicht auf Eingaben
 * reagieren.
 *******************************************************************/
void BrakeTest::start()
{
    /*
     * TODO Aufgabe 1.4
     *      Bestimmen Sie die Beschleunigung.
     *      Beschleunigen (positive Beschleunigung) und Bremsen (negative Beschleunigung) sind betragsmäßig gleich.
     *      Hier soll die Beschleunigung beim Bremsen anhand der dabei zurückgelegten Wegstrecke (Bremsweg) gemessen werden,
     *      da das Ende des Beschleunigungsvorgangs nicht genau bestimmt werden kann während das Ende des Bremsvorgangs
     *      leichter erkannt wird.
     */
    float startbrake = 0.0, brakedist = 42.0;   // Distanz zu Beginn Bremsvorgangs und Bremsweg
    float a = 42.0;                             // Variable zur Berechnung der Beschleunigung

    /* Messen, wie viel Platz geradeaus zur Verfügung steht und umdrehen, wenn weniger als 50.0 (m)
     * Hinweis: Warten bis der Roboter sich ganz gedreht hat! */
    // vvvv--
    // ^^^^--

    /* Wegstreckenzähler auf 0 setzen und mit p_speed (m/s) starten */
    // vvvv--
    // ^^^^--

    /* Warten, bis Zielgeschwindigkeit erreicht */
    // vvvv--
    // ^^^^--

    /* Streckenzähler speichern und Bremsung beginnen */
    // vvvv--
    // ^^^^--

    /* Warten, bis der Roboter stehen geblieben ist */
    // vvvv--
    // ^^^^--

    /* Bremsweg und Beschleunigung berechnen  */
    // vvvv--
    // ^^^^--

    /* Ergebnisse ausgeben */
    p_robot->message("BrakeTest: speed=" + std::to_string(p_speed) + " dist=" + std::to_string(brakedist)
                        + " accel=" + std::to_string(a));
}

/*******************************************************************
 *
 * BrakeTest::stop()
 *
 *------------------------------------------------------------------
 * Parameter:
 *   none
 * Rückgabewert:
 *   none
 *------------------------------------------------------------------
 * Diese Funktion für das Beenden des Verhaltens wird nicht benötigt.
 *******************************************************************/
void BrakeTest::stop()
{
    // nothing
}
