/*
 * #######################################################################################
 * ##    Filename:  behavior.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#include "behavior.h"


Behavior::Behavior(std::string param) :
        p_robot(NULL),
        p_param(param)
{}


void Behavior::setRobot(BehavingRobot *robot)
{
    p_robot = robot;
}
