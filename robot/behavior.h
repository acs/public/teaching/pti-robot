/*
 * #######################################################################################
 * ##    Filename:  behavior.h
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#ifndef BEHAVIOR_H
#define BEHAVIOR_H

#include<string>

#include "behavingrobot.h"

class BehavingRobot;

class Behavior
{

public:
    Behavior(std::string param = "");

    void setRobot(BehavingRobot *robot);

    virtual void start() = 0;
    virtual void stop() = 0;

protected:
    BehavingRobot *p_robot;
    std::string p_param;

};

#endif // BEHAVIOR_H
