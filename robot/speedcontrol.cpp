/*
 * #######################################################################################
 * ##    Filename:  speedcontrol.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#include "speedcontrol.h"

#include <cmath>

SpeedControl::SpeedControl(std::string param) :
    TimedBehavior(param)
{
    p_speed = std::stof(p_param);
}


void SpeedControl::init()
{
    p_speed_kurve = sqrt(4 * p_robot->radius() * p_robot->accel());
    if (p_speed < p_speed_kurve) p_speed = p_speed_kurve;

    /* find nearest wall */
    float f,l,r,b;
    f = p_robot->lineSensor(0);
    l = p_robot->lineSensor(-90);
    r = p_robot->lineSensor(90);
    b = p_robot->lineSensor(180);

    if (b < l && b < r && b < f) p_robot->turn(180);
    else if (l < r && l < f) p_robot->turn(-90);
    else if (r < f) p_robot->turn(90);

    p_robot->speed(p_speed);
    p_state = 0;
}

void SpeedControl::task() // 100 Hz
{
    float vc = p_robot->getSpeedAsync();

    float df = p_robot->lineSensor(0);

    switch (p_state) {
    case 0 : /* search first wall */
        if (df < 4 * p_robot->radius()) {
            p_robot->turn(90);
            p_robot->distanceReset();
            p_state = 1;
        }
        break;
    case 1 : /* wait for end of turn */
        if (df < 4*p_robot->radius()) {
            p_robot->turn(90);
            p_state = 2;
        }
        if (p_robot->distance() > M_PI_2 * 2 * p_robot->radius()) {
            p_state = 2;
        }
        break;
    case 2 :

        if (df < 4 * p_robot->radius() + 0.02*vc) {
            p_robot->turn(90);
            p_robot->distanceReset();
            p_state = 1;
            return;
        }

        float dl = p_robot->lineSensor(-90);
        if (dl > 2.5 * p_robot->radius() + 0.02*vc) {
            p_robot->turn(-90);
            p_robot->distanceReset();
            p_state = 1;
            return;
        }

        float sb = 0;
        if (vc > p_speed_kurve) sb = (vc * vc - p_speed_kurve * p_speed_kurve) / (2*p_robot->accel());

        float alpha = 0;
        float l = 0;
        float da = 0;
        if (sb > 0) {
            alpha = atan(dl/sb) * 180.0 / M_PI;
            l = sqrt(dl * dl + sb * sb);
            da = p_robot->lineSensor(-alpha);
        } else {
            l = dl;
            da = dl;
        }

        if ((df > sb + 4*p_robot->radius() + 0.02*vc) && std::abs(da - l) < 1.0) {
            p_robot->message("accel! (df=" + std::to_string(df) + " sb=" + std::to_string(sb));
            p_robot->speed(p_speed);
        } else {
            p_robot->message("brake! (df=" + std::to_string(df) + " sb=" + std::to_string(sb));
            p_robot->speed(p_speed_kurve);
        }


        break;
    }

}

void SpeedControl::finalize()
{
    p_robot->speed(.0);
}
