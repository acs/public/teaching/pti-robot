/*
 * #######################################################################################
 * ##    Filename:  timedbrakecontrol.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS)
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#include "timedbrakecontrol.h"
#include <cmath>

TimedBrakeControl::TimedBrakeControl(std::string param) :
        TimedBehavior(param)
{
    p_max_speed = std::stof(p_param);
}

void TimedBrakeControl::init()
{
    /*
     * TODO Aufgabe 2.2
     *      Beschleunigen Sie auf <p_max_speed / 2>
     */
    // vvvv--
    // ^^^^--
}

void TimedBrakeControl::task() // 100 Hz
{
    /*
     * TODO Aufgabe 2.2
     *      Messen Sie die aktuellen Geschwindigkeit (mit getSpeedAsync() !)
     *      und berechnen Sie daraus die momentane kritische Distanz
     *      Bremsen Sie, wenn die freie Distanz nach vorne weniger als die kritische Distanz <critical_dist> ist.
     */
    float act_speed;        // momentane Geschwindigkeit
    float critical_dist;    // kritische Distanz

    // vvvv--
    // ^^^^--
}

void TimedBrakeControl::finalize()
{
    // beim Beenden: Roboter anhalten.
    p_robot->speed(.0);
}
