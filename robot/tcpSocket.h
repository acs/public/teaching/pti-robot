/*
 * #######################################################################################
 * ##    Filename:  tcpSocket.h
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##      Author:  Felix Wege (ACS)
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##
 * #######################################################################################
 */

#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include <string>


class TcpSocket
{

public:
    // Con-/Destructor
    TcpSocket();
    TcpSocket(const char* serveraddress, int port);
    ~TcpSocket();

    void init(const char* serveraddress, int port);

    int connectToHost();
    int connectToHost(const char* serveradress, int port);
    int disconnectFromHost();    

    // Communication with remote host
    void sendString(std::string _message);
    std::string receiveString();

    // Bidirectional communication with remote host
    std::string dialog(std::string _message);
  

private:
    // Connect/Disconnect
    int _open();
    int _close();

    struct sockaddr_in * sockAddrPtr; // Pointer to socket struct

    int MAXBUFF = 4096; // defines max length of received message
    int MAXMSG = 255;   // defines max length of send message
    
    int _socket;
    bool connected = false;
    bool initialized = false;
};

#endif