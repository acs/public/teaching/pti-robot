/*
 * #######################################################################################
 * ##    Filename:  behavingrobot.h
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#ifndef BEHAVINGROBOT_H
#define BEHAVINGROBOT_H

#include "robot.h"
#include "behavior.h"

class Behavior;

class BehavingRobot : public Robot
{
public:
    BehavingRobot(CLI* p_cli);
    ~BehavingRobot();

    void setBehavior(Behavior *value);
    void stop();

private:
    Behavior *p_behavior;
};

#endif // BEHAVINGROBOT_H
