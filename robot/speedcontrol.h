/*
 * #######################################################################################
 * ##    Filename:  speedcontrol.h
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#ifndef SPEEDCONTROL_H
#define SPEEDCONTROL_H

#include "timedbehavior.h"

class SpeedControl : public TimedBehavior
{
public:
    explicit SpeedControl(std::string param);

private:
    virtual void init();
    virtual void task();
    virtual void finalize();


private:
    float p_speed;
    float p_speed_kurve;

    int p_state;

};

#endif // SPEEDCONTROL_H
