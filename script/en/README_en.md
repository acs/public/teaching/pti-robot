# Labcourse Computer Engineering - Experiment 2<br>Distributed and Real-Time Capable Systems

![](../../.images/ascii_header.png)

[> German Version <](../../README.md)

This exercise introduces the basics and programming of distributed and real-time systems. A robot simulation is used as the distributed system: any number of clients can connect to the server, and the server simulates a robot for each client, all of which move within an arena. The clients can query their robot's sensors and send control commands over the network. The basics that need to be considered in real-time programming are taught using this example.

For this purpose, students independently program and further develop the robot and its corresponding Command Line Interface (CLI). The required source code, which contains designated gaps and tasks, is available in this repository. Additionally, an up-to-date version of the script, instructions, and the addresses used for the graphical interface (Observer) and arena (Server) are provided below.

> **Hint:** Solutions for common problems can be found here: [Troubleshooting](#troubleshooting)

## Table of Contents
- [Labcourse Computer Engineering - Experiment 2<br>Distributed and Real-Time Capable Systems](#labcourse-computer-engineering---experiment-2distributed-and-real-time-capable-systems)
  - [Table of Contents](#table-of-contents)
  - [Adresses](#adresses)
  - [Directory Structure](#directory-structure)
  - [How-To](#how-to)
    - [Dependencies](#dependencies)
    - [Compiling the Robot Application](#compiling-the-robot-application)
    - [Starting the Robot Application](#starting-the-robot-application)
  - [Troubleshooting](#troubleshooting)
    - [Observer does not display robot/arena correctly...](#observer-does-not-display-robotarena-correctly)
    - [Robot cannot connect to Arena...](#robot-cannot-connect-to-arena)
    - [Compiler not installed...](#compiler-not-installed)
  - [Contact](#contact)

## Adresses

|  Name | Description | Adress | Port |
|:-:|:-:|:-:|:-:|
|Observer|Graphical Real-Time Visualization of the Arena| https://observer.k8s.eonerc.rwth-aachen.de/?wss:arena.k8s.eonerc.rwth-aachen.de |/|
|Arena|Server that robots can connect to| arena.k8s.eonerc.rwth-aachen.de | 30352 |

## Directory Structure

- `common`: Definitions that have to be known in all parts of the programm, i.e., Robot, Observer and Arena
- `robot`: Source code of the Robot application
- `script`: Current version of the script, including supplementary material

## How-To
### Dependencies
To compile and build the robot applications, a suitable compiler (g++) and the build management tool (make) are required. On Ubuntu, these can be easily installed using the following command:
```bash
sudo apt update && sudo apt install make g++
```

### Compiling the Robot Application
Open a terminal using `<CTRL>`+`<ALT>`+T and navigate to the directory of the robot application using the "change directory" command (cd).

```
cd <path_to_robot>
```
Replace `<path_to_robot>` with the path to the directory where you have unpacked the robot code.

The robot application can now be compiled and built by running the build management tool make. Use the command:
```
make all
```
or the shorter version:
```
make
```

### Starting the Robot Application
If the application was successfully built, the command-line-based interface can be started using:
```
./robot
```
After starting, you will first need to enter a name, as well as the address and port of the arena. To avoid having to repeatedly enter this information during the exercise, you can also provide this information as additional parameters when starting the application. Use the command:
```
./robot <name> <address> <port>
```
Replace `<name>`, `<address>`, and `<port>` accordingly.
By using the arrow keys(`↑`|`↓`)  you can scroll through the most recently executed commands in the terminal, allowing you to avoid retyping this function call and the required information.

> **IMPORTANT:** To establish a connection between the robot and the arena, you must be connected to the RWTH network (e.g., using the RWTH VPN)!

## Troubleshooting
### Observer does not display robot/arena correctly...

![](../../.images/observer-troubleshooting.png)

|||
|:-:|:-|
|**Error**| *SOCKET-ERROR: -1 (QAbstractSocket::SocketError)* <br> The observer loads but does not display any walls or robots.|
|**Cause**| Connection between observer and arena is faulty.|
|**Solution**| Check if the correct address of the raena has been selcted, see [Adresses](#adresses)|

### Robot cannot connect to Arena...

|||
|:-:|:-|
|**Error**| *ERROR: dialog() - send() returned -1* <br> Robot throws an error and is not displayed in the arena.|
|**Cause**| Connction between robot and arena is faulty.|
|**Solutions**| 1) Ensure that an internet connection is available<br>2) Ensure that you are connected to the RWTH network (e.g., using VPN)<br>3) When starting the robot, use one of the following IPs instead of *arena.k8s.eonerc.rwth-aachen.de* <br>        137.226.248.61<br>        137.226.248.62<br>        137.226.248.63|
> Example: `./robot MyRobot 137.226.248.61 30352`

### Compiler not installed
|||
|:-:|:-|
|**Error**| *make: g++: No such file or directory* <br> Robot cannot be build and throws an error when executing `make`|
|**Cause**| The required Compiler `g++` is not correctly intalled.|
|**Solution**| Ensure that the compiler is correctly installed. See [HowTo: Dependencies](#dependencies)|

### macOS: clang unknown argument '-fconcepts'
|||
|:-:|:-|
|**Error**| *clang++: error: unknown argument: '-fconcepts'* <br> On macOS: Robot cannot be build and throws an error when executing `make`|
|**Cause**| By default, macOS uses `clang` instead of `g++`, which does not support the flags given by our Makefile.|
|**Solution**| 1) Install `g++` using homebrew `homebrew install g++`<br>2) Modify the compiler variable (`CC`) in the Makefile to match your `g++` version, e.g., `CC = g++-13`|

## Contact:
[![](../../.images/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

Felix Wege - fwege@eonerc.rwth-aachen.de <br>
Sahnawaz Alam - sahnawaz.alam@eonerc.rwth-aachen.de